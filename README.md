# socat

## Example
redirect all port $PORT connections to unix domain socket $SOCKET_FILE
```
docker run -p $PORT:8080 -v $SOCKET_FILE:/source.sock vancez/socat tcp-listen:8080 unix-connect:/source.sock
```

redirect all unix domain socket /path/dest.sock connections to port container:8080
```
docker run -v /path:/data vancez/socat tcp-connect:container:8080 unix-listen:/data/dest.sock
```
